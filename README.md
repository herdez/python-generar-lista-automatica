## Generar lista automática con Python

Para este ejercicio es necesario documentarse acerca de como generar numeros aleatorios con Python. 

Definir la función `random_list` que recibe una lista de estudiantes `alumnis` y genera de manera automática una lista de `n` calificaciones entre un valor mínimo `min_value` y un valor máximo `max_value` para cada estudiante. Considera las listas generadas en el `driver code` como ejemplo para validar la función.

Es importante entregar una función con el uso de `list comprehensions` y otra función con el uso tradicional del `for` loop.

```python
"""random_list function"""



#driver code

"""Ejemplo 1"""
print(random_list(["Robert", "Charlis", "Marco", "Pepe", "Angi"], 7, 5, 10))

[['Robert', [5, 8, 5, 7, 5, 9, 8]], ['Charlis', [5, 8, 10, 8, 7, 7, 10]], ['Marco', [9, 5, 10, 8, 8, 5, 8]], ['Pepe', [6, 7, 5, 7, 8, 6, 8]], ['Angi', [7, 6, 7, 8, 9, 7, 7]]]

"""Ejemplo 2"""
print(random_list(["Faby", "Moni", "Stefani", "Herni"], 5, 1, 10))

[['Faby', [8, 10, 8, 1, 9]], ['Moni', [7, 10, 6, 6, 8]], ['Stefani', [1, 8, 8, 10, 5]], ['Herni', [9, 8, 9, 7, 5]]]


```

